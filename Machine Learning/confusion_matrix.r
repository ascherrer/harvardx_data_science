# HarvardX: PH125.8x Data Science: Machine Learning
# Basics of Evaluating Machine Learning Algorithms  
# Confusion Matrix

# Loading
library(dplyr)
library(dslabs)
library(caret)
library(lubridate)

# Comprehension Check: Confusion Matrix 

data("reported_heights")

dat <- mutate(reported_heights, date_time = ymd_hms(time_stamp)) %>%
  filter(date_time >= make_date(2016, 01, 25) & date_time < make_date(2016, 02, 1)) %>%
  mutate(type = ifelse(day(date_time) == 25 & hour(date_time) == 8 & between(minute(date_time), 15, 30), "inclass","online")) %>%
  select(sex, type)

y <- factor(dat$sex, c("Female", "Male"))
x <- dat$type

#----------
#Question 1
#----------

dat %>% group_by(type) %>% summarize(prop_female = mean(sex == "Female"))

#----------
#Question 2
#----------

y_hat <- ifelse(x == "inclass", "Female", "Male") %>% factor(levels = levels(y))
confusionMatrix(data = y_hat, reference = y)

#----------
#Question 3
#----------

table(predicted = y_hat, actual = y)

#----------
#Question 4
#----------

sensitivity(y_hat, y)

#----------
#Question 5
#----------

specificity(y_hat, y)

#----------
#Question 6
#----------

mean(y == "Female")

