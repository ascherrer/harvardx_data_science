# HarvardX: PH125.8x Data Science: Machine Learning
# Basics of Evaluating Machine Learning Algorithms  
# Pratice with Machine Learning

# Loading
library(dplyr)
library(dslabs)
library(caret)
library(purrr)

data(iris)
iris <- iris[-which(iris$Species=='setosa'),]
y <- iris$Species

#----------
#Question 1
#----------

set.seed(2)
test_index <- createDataPartition(y, times=1, p=0.5, list=FALSE)
test <- iris[test_index,]
train <- iris[-test_index,]

#----------
#Question 2
#----------

foo <- function(x){
  rangedValues <- seq(range(x)[1],range(x)[2],by=0.1)
  sapply(rangedValues,function(i){
    y_hat <- ifelse(x>i,'virginica','versicolor')
    mean(y_hat==train$Species)
  })
}
predictions <- apply(train[,-5],2,foo)
sapply(predictions, max)	

#----------
#Question 3
#----------

cutoff <- seq(1, 5, 0.1) 

# Calculate accurancy with cutoff on the training set
accuracy <- map_dbl(cutoff, function(x){
  y_hat <- ifelse(train$Petal.Length > x, "virginica", "versicolor") %>% 
    factor(levels = levels(test$Species %>% droplevels()))
  mean(y_hat == train$Species %>% droplevels())
})

# Plot the accuracy obtained on the training set
plot(cutoff, accuracy)

# Print maximum value obtained on the trainin set
max(accuracy)

# Print the best cutoff obtained on the trainin set
best_cutoff <- cutoff[which.max(accuracy)]
print(best_cutoff)

# Test the best_cutoff on the test set
y_hat <- ifelse(test$Petal.Length > best_cutoff, "virginica", "versicolor") %>% 
  factor(levels = levels(test$Species %>% droplevels()))
y_hat <- factor(y_hat)

# Print the accurancy obtained on the test set
mean(y_hat == test$Species %>% droplevels())

#----------
#Question 5
#----------

# Calculate accurancy with cutoff on the training set for Petal.Length
accurancy_Petal_Length <- map_dbl(cutoff, function(x){
  y_hat <- ifelse(train$Petal.Length > x, "virginica", "versicolor") %>% 
    factor(levels = levels(test$Species %>% droplevels()))
  mean(y_hat == train$Species %>% droplevels())
})

# Calculate accurancy with cutoff on the training set for Petal.Width
accurancy_Petal_Width <- map_dbl(cutoff, function(x){
  y_hat <- ifelse(train$Petal.Width > x, "virginica", "versicolor") %>% 
    factor(levels = levels(test$Species %>% droplevels()))
  mean(y_hat == train$Species %>% droplevels())
})

# Print the best cutoffs obtained on the trainin set
best_cutoff_Length <- cutoff[which.max(accurancy_Petal_Length)]
best_cutoff_Width <- cutoff[which.max(accurancy_Petal_Width)]
print(best_cutoff_Length)
print(best_cutoff_Width)

# Test the best_cutoff_Length and best_cutoff_Width on the test set
y_hat <- ifelse(test$Petal.Length > best_cutoff_Length | train$Petal.Width > best_cutoff_Width, "virginica", "versicolor") %>% 
  factor(levels = levels(test$Species %>% droplevels()))
y_hat <- factor(y_hat)

# Print the accurancy obtained on the test set
mean(y_hat == test$Species %>% droplevels())
                          
